<?php

class CashDistributorTest extends PHPUnit_Framework_TestCase
{
    public function assertPreConditions()
    {
        $this->assertTrue(class_exists(
            $class = 'CashDistributor'), 'Class not found: ' . $class);

        $this->assertClassHasAttribute('availableBills', 'CashDistributor');

        $this->assertTrue(class_exists($class = 'InvalidWithdrawException'),
            'Class not found: ' . $class);
    }

    public function invalidWithdrawProvider()
    {
        return [
            [0],
            [1],
            [3],
        ];
    }

    public function validWithdrawProvider()
    {
        return [
            [2, [2 => 1]],
            [4, [2 => 2]],
            [6, [2 => 3]],
            [7, [5 => 1, 2 => 1]],
            [8, [2 => 4]],
            [9, [5 => 1, 2 => 2]],
            [12, [10 => 1, 2 => 1]],
            [13, [5 => 1, 2 => 4]],
            [20, [20 => 1]],
            [22, [20 => 1, 2 => 1]],
            [25, [20 => 1, 5 => 1]],
            [26, [20 => 1, 2 => 3]],
            [36, [20 => 1, 10 => 1, 2 => 3]],
            [50, [50 => 1]],
            [52, [50 => 1, 2 => 1]],
            [53, [20 => 2, 5 => 1, 2 => 4]],
            [54, [50 => 1, 2 => 2]],
            [55, [50 => 1, 5 => 1]],
            [51, [20 => 2, 5 => 1, 2 => 3]],
            [70, [50 => 1, 20 => 1]],
            [96, [50 => 1, 20 => 2, 2 => 3]],
            [102, [100 => 1, 2 => 1]],
            [103, [50 => 1, 20 => 2, 5 => 1, 2 => 4]],
            [105, [100 => 1, 5 => 1]],
            [120, [100 => 1, 20 => 1]],
            [132, [100 => 1, 20 => 1, 10 => 1, 2 => 1]],
            [178, [100 => 1, 50 => 1, 20 => 1, 2 => 4]],
            [252, [100 => 2, 50 => 1, 2 => 1]],
            [296, [100 => 2, 50 => 1, 20 => 2, 2 => 3]],
        ];
    }

    public function testShouldInstantiateWithoutArguments()
    {
        $cashDistributor = new CashDistributor();
        $this->assertInstanceOf('CashDistributor', $cashDistributor);
    }

    /**
     * @dataProvider invalidWithdrawProvider
     * @expectedException InvalidWithdrawException
     */
    public function testShouldThrowExceptionForInvalidWithdraw($withdrawAmount)
    {
        $cashDistributor = new CashDistributor();
        
        $returnedBills = $cashDistributor
            ->getMinimalAmountOfBills($withdrawAmount);
    }

    /**
     * @dataProvider validWithdrawProvider
     */
    public function testShouldReturnMinimumAmountOfBillsForValidWithdraw(
        $withdrawAmount, $expectedBills
    ) {
        $cashDistributor = new CashDistributor();

        try {
            $returnedBills =
                $cashDistributor->getMinimalAmountOfBills($withdrawAmount);
        } catch(InvalidWithdrawException $e) {
            $this->fail('Unexpected exception thrown: ' . $e->getMessage());
        }

        $this->assertEquals($expectedBills, $returnedBills,
            "Invalid bill distribution for the given value ($withdrawAmount):");
    }
}